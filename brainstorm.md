# Brainstorm on the Future of Grex.org

Several Grex users were discussing on Agora recently and a couple years ago about the future of Grex; about whether Grex should attempt a new user drive, and about what changes Grex might need to make in order to successfully increase activity on the system. Grex user cmccabe had previously been building a vision of a text-based social computing platform recently, and the original draft of this document was his braindump of some of those ideas, in hopes they would of interest to those who want to reinvigorate Grex.

Grex's already has a great set of core principles and they're a solid foundation ontop of which to consider any type of modernization. The goal of contributing to "a better-informed citizenry" is the most important, but also one that begs the question of how to keep plans within an achievable scope.

## Objective

Build on Grex's existing mission of education and charity, and explicitly add the goal of keeping social computing technology available and engaging to the public. (Note: the charity goal is noble, but what does it mean if Grex is not a geographically focused entity?)

## Identity

Reading the history of Grex, it is clear what the identity was.  "A public service promoting free speech and public access on the Internet."  How about maintaining that core identity and revolving it more explicitly around a UNIX-based social network concept?  If so, the change might involve adopting some of the hackerspace/makerspace model.

A social computing platform for people who see text-based, *NIX computing as a great alternative to the distraction and sensationalism of web-based social media. Also, a place where people can teach, practice and learn *NIX skills. In the spirit of -learning-, a core value of the community should be individual effort to research, experiment and learn -- while using the community for tips an guidance when necessary.

## Goals and Not Goals

### Goals

* create a welcoming, encouraging place for anyone who wants to respectfully socialize in a *NIX environment,
* provide access to all the basic *NIX utilities that one needs to learn through small educational projects,
* provide several options for social interaction -- party, some kind of bbs, IRC, maybe others --like several different rooms in a community recreation center,
* instantiate an apprenticeship model where users who want to help out can move up in rank by demonstrating skills and sincere commitment to the system and local community -- higher rank accompanied by increased system access (and responsibilities). Other, normal, users can simply enjoy the services the system has to offer.
* supporting the apprentciceship model, develop a community-maintained curriculum* of the skills and knowledge a well-informed top-rank user must have (both technical, social and political).
* through the apprenticeship model, build a layer of trusted people with *NIX sysadmin skills who are willing to volunteer for system upkeep
* register the system as a non-profit [update: it already is, 501(c)(3)] and charge nominal membership fees that will cover some of the hard costs of maintenance
* encourage (as a central theme to the system/community) thoughtful, philosophical and practical dicussion around the role and value of non-commercial social internet, and around methods for promoting it. And, of course, provide text-based forums for thoughtful, intellectual conversations on all sorts of topics.

### Not Goals

* provide point-and-click interfaces to the command-line utilities of the system; or conversely, a goal could be to provide only enough that they can be used as stepping stone for those who want to *learn* how to go full command-line
* permit hactivism (if the system/community has any shared ideals, they should be peacefully promoted)
be bound to any geographic location -- although encouragement of propinquitous users to meet up (e.g. in Ann Arbor) is fine.
* Offer users massive compute power, or huge amounts of storage (unless through a cloud model, those could be offered for a usage fee).

## Curriculum

In no particular order, some possible curriculum topics:

* IT security of a multi-user machine
* personal IT security; public-key encryption, etc.
* basic skills for a *NIX user
* *NIX system admin fundamentals
* basic software development (possibly language agnostic)
* familiarity with the debates around free open-source software vs proprietary software
* social science analysis of corporate social media vs non-commercial social computing
* familiarity with the history of Grex itself

...information on all these topics could all be spelled out in detailed tutorials; but that would entail considerable effort for upkeep, and it would discourage users from using the broader set of available resources on the net or in their local libraries. Instead, the community could simply say "here are some topics that we think are important" -- and then use the system's social forums to provide encouragement, tips and guidance to users who put in the effort to learn and ask questions.